import { html, css, LitElement } from "lit";

import "@polymer/paper-input/paper-input";
import "@polymer/iron-input/iron-input";

export class ChatList extends LitElement {
  static get styles() {
    return css`
      .container {
        margin: 15px 15px 10px 15px;
        width: 280px;
      }
      .header {
        display: flex;
        align-items: center;
        justify-content: flex-start;
        gap: 5px;
      }
      .contact {
        display: flex;
        width: 100%;
        margin: 0 0 0 5px;
      }

      h2 {
        font-size: 12px;
      }

      .content {
        display: flex;
        margin: 0px 0px 5px 5px;
        flex-wrap: wrap;
        flex-direction: column;
      }
      .content h2 {
        font-size: 10px;
        margin-bottom: -5px;
        text-align: left;
      }

      .content p {
        font-size: 8px;
        text-align: left;
        max-width: fit-content;
      }

      .detail {
        display: inline-flex;
        align-items: center;
        gap: 5px;
      }

      .detail .notification {
        display: inline-flex;
        justify-content: center;
        align-items: center;
        background-color: gray;
        border-radius: 10px;
        height: 15px;
        width: 15px;
        color: white;
        font-weight: bold;
      }

      .avatar img {
        width: 15px;
        height: 15px;
        border-radius: 50px;
      }

      .search {
        max-width: fit-content;
      }
    `;
  }

  static get properties() {
    return {
      profile: {
        type: Object,
      },
      contacts: {
        type: Array,
      },
    };
  }

  constructor() {
    super();
    this.contacts = [
      {
        avatar:
          "https://cdn.icon-icons.com/icons2/2201/PNG/512/telegram_logo_circle_icon_134012.png",
        name: "Telegram",
        lastMessage: {
          text: "Tienes una actualización",
          time: `${new Date().getHours()}:${new Date().getMinutes()}`,
        },
        messages: [
          {
            seen: false,
          },
        ],
      },
      {
        avatar:
          "https://cdn.icon-icons.com/icons2/2201/PNG/512/telegram_logo_circle_icon_134012.png",
        name: "Telegram",
        lastMessage: {
          text: "Tienes una actualización",
          time: `${new Date().getHours()}:${new Date().getMinutes()}`,
        },
        messages: [
          {
            seen: false,
          },
        ],
      },
      {
        avatar:
          "https://cdn.icon-icons.com/icons2/2201/PNG/512/telegram_logo_circle_icon_134012.png",
        name: "Telegram",
        lastMessage: {
          text: "Tienes una actualización",
          time: `${new Date().getHours()}:${new Date().getMinutes()}`,
        },
        messages: [
          {
            seen: false,
          },
        ],
      },
    ];
    this.profile = {};
  }

  onContactSelect(contact) {
    this.dispatchEvent(
      new CustomEvent("contact-selected", {
        detail: contact,
      })
    );
  }

  render() {
    return html`
      <div class="container">
        <div class="header avatar">
          <img src="${this.profile.avatar}" alt="${this.profile.name}" />
          <h2>${this.profile.name}</h2>
        </div>
        <paper-input class="search" type="text" label="Buscar"></paper-input>
        ${this.contacts.map(
          (contact) => html`
            <div
              class="contact"
              @click="${() => {
                this.onContactSelect(contact);
              }}"
            >
              <div class="avatar">
                <img src="${contact.avatar}" alt="${contact.name}" />
              </div>
              <div class="content">
                <h2>${contact.name}</h2>
                <div class="detail">
                  <p>${contact.lastMessage.time}</p>
                  <span class="notification">
                    <p>
                      ${contact.messages.filter((message) => !message.seen)
                        .length}
                    </p>
                  </span>
                </div>
                <p>${contact.lastMessage.text}</p>
              </div>
            </div>
          `
        )}
      </div>
    `;
  }
}
