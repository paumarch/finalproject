import { html, css, LitElement } from 'lit';

export class MylistComponent extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        padding: 25px;
        color: var(--mylist-component-text-color, #000);
      }
    `;
  }

  static get properties() {
    return {
      title: { type: String },
    };
  }

  constructor() {
    super();
    this.title = 'My List Component';
  }

  render() {
    return html`
      <h2>${this.title}</h2>
      <button>Back</button>
    `;
  }
}
