import { html, css, LitElement } from "lit";

import "@polymer/paper-input/paper-input";
import "@polymer/paper-button/paper-button";

export class ChatBox extends LitElement {
  static get styles() {
    return css`
      .container {
        margin: 15px 15px 10px 15px;
      }

      .header h2 {
        color: darkblue;
      }

      .header {
        display: flex;
        align-items: center;
        gap: 25px;
      }

      .header img {
        border-radius: 50px;
        max-height: 20px;
        max-width: 20px;
      }

      .avatar {
        width: 20px;
        height: 20px;
        border-radius: 50px;
      }

      .message {
        border-radius: 10px;
        padding: 3px;
        margin: 5px;
        background-color: #ffffff4c;
      }

      .message h3 {
        margin-left: 5px;
        font-size: 12px;
        color: darkblue;

      }

      .message p {
        margin-left: 5px;
        font-size: 10px;
        color: darkblue;
      }

      .write {

      }

      .footer {
        /* position: absolute;
        min-width: 550px;
        top: 430px; */
        display: flex;
        flex-direction: column;
      }

    `;
  }

  static get properties() {
    return {
      contact: {
        type: Object,
      },


    };
  }

  constructor() {
    super();
    this.contact = {
      name: "",
      avatar: "",
      messages: [],
    };
  }

  onTelegramSent(){
    const sendTelegram = this.shadowRoot.querySelector('paper-input')
    if (sendTelegram.value === '') return;
    const newTelegram = {
      text: sendTelegram.value,
      owner: {
        name: 'Pau March'
      }
    };
    this.dispatchEvent(new CustomEvent('telegram-sent', {detail: newTelegram}))
    sendTelegram.value = '';
  }

  render() {
    return html`
      <div class="container">
        <div class="header">
          <img src="${this.contact.avatar}" />
          <h2>${this.contact.name}</h2>
        </div>
        <div class="content">
          ${this.contact?.messages?.map(
            (message) => html`
              <div class="message">
                <h3>${message.owner.name}</h3>
                <p>${message.text}</p>
              </div>
            `
          )}
        </div>
        <div class="footer">
          <paper-input class="write" label="Escribir mensaje"></paper-input>
          <paper-button class="send" @click="${this.onTelegramSent}">Enviar</paper-button>
        </div>
      </div>
    `;
  }
}
