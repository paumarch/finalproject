import { html, css, LitElement } from "lit";

import "@polymer/paper-dropdown-menu/paper-dropdown-menu";
import "@polymer/paper-item/paper-item.js";
import "@polymer/paper-listbox/paper-listbox.js";

export class I18nTranslate extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        color: var(--i18n-translate-text-color, #000);
      }
    `;
  }

  static get properties() {
    return {
      title: { type: String },
      country: { type: Object },
      lang: { type: String },
      intlCode: { type: String },
      description: { type: String },
    };
  }

  constructor() {
    super();

    // Definimos un Map por cada país que mostrará un idioma.

    this.country = new Map();

    // Dentro usaremos los métodos Set y Get para definir cada texto a traducir.
    // El Objeto Country contiene un String con el nombre del país
    // y un Array con los textos a traducir.

    this.country.set("España", [
      "Hola !",
      "Español",
      "es-ES",
      "Bienvenido a i18n",
    ]);
    this.country.set("France", [
      "Salut !",
      "Français",
      "fr-FR",
      "Bienvenue sur i18n",
    ]);
    this.country.set("Portugal", [
      "Olá !",
      "Português",
      "pt-PT",
      "Bem vindo ao i18n",
    ]);

    // Ahora definiremos cada texto a traducir según su posición dentro del Array
    // a través del método Get del Map.

    this.title = this.country.get("España")[0];
    this.lang = this.country.get("España")[1];
    this.intlCode = this.country.get("España")[2];
    this.description = this.country.get("España")[3];
  }

  // Ahora la función para traducir los textos.

  translate(event) {
    if (event.detail?.value !== "") {
      const values = this.country.get(event.detail.value);
      this.title = values[0];
      this.lang = values[1];
      this.intlCode = values[2];
      this.description = values[3];
    }
  }

  // Array.from() es un método para devolver un Array desde un objeto iterable.

  // this.country.keys() devuelve un objeto iterable con cada elemento del map.
  // Y el .mmap a continuación devuelve cada Key por separado, que se mete dentro
  // de una lista, por el momento.

  render() {

    return html`
      <h2>${this.title}</h2>
      <paper-dropdown-menu label="¿De dónde eres?" @value-changed="${this.translate}">
        <paper-listbox slot="dropdown-content" selected="0">
          ${Array.from(this.country.keys()).map(
            (key) => html`<paper-item>${key}</paper-item>`
          )}
        </paper-listbox>
      </paper-dropdown-menu>
      <div>
        <p>
        Según el país que hayas escogido, tu idioma aparecerá aquí: <b>${this.lang}</b>.
        Cada idioma utiliza un propio código para identificarse, el tuyo es: <b>${this.intlCode}</b>.
        Y un ejemplo de frase en tu idioma sería: <b>${this.description}</b>.</p>
        </div>
    `;
  }
}
