import { LitElement, html, css } from "lit";

import "@minsait/apple-component";
import "@minsait/lock-screen";
import "@minsait/photos-component";
import "@minsait/streaming-app";
import "@minsait/spotify-app";
import "@minsait/telegram-app";
import "@vaadin/vaadin-icon";

const logo = new URL("../assets/Apple_Logo.svg", import.meta.url).href;

// const onApplicationSelected = (event) => {
//   const app = event.detail;
//   const components = document.querySelectorAll("app-ios");
//   // this.page = app.name;
//   for (const component of components) {
//     component.applications = component.applications.map((application) => {
//       if (application.name === app.name) {
//         application.notifications -= 1;
//         console.log(application);
//       }

//       if (app.name === "Messages") {
//         this.gotoPage("telegram");
//       }
//       return application;
//     });
//   }
//   console.log(app.url);
// };

export class AppIos extends LitElement {
  static get properties() {
    return {
      title: { type: String },
      page: { type: String },
      applications: { type: Array },
      songs: { type: Array },
      onApplicationSelected: { type: Function },
    };
  }

  static get styles() {
    return css`
      /* Inicio de CSS Contenedor */

      :host {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: flex-start;
        font-size: calc(10px + 2vmin);
        color: #1a2b42;
        max-width: 480px;
        margin: 0 auto;
        text-align: center;
        background-color: var(--app-ios-background-color);
        color: #ffffff;
      }

      p {
        font-size: 10px;
      }

      .container {
        display: flex;
        width: 400px;
        height: 915px;
      }
      ._button {
        border: none;
        padding: 0;
        outline: none;
        background-color: transparent;
        user-select: none;
      }

      span {
        box-sizing: border-box;
      }

      .screen {
        background-color: #4158d0;
        background-image: linear-gradient(
          43deg,
          #4158d0 0%,
          #c850c0 46%,
          #ffcc70 100%
        );

        display: flex;
        flex-wrap: wrap;
        justify-content: center;
        padding: 15px;
        max-width: 350px;
        min-width: 350px;
        min-height: 400px;
        max-height: 495px;
        overflow-y: auto;
      }

      .iphone,
      .screen {
        border: 2px solid #525c6b;
      }

      .iphone {
        padding: 60px 10px;
        border-radius: 25px;
        position: relative;
      }

      .home,
      .camera,
      .proximity,
      .speaker,
      .silence,
      .volume-up,
      .volume-down,
      .sleep {
        position: absolute;
      }

      .home,
      .camera,
      .proximity {
        border-radius: 50%;
      }

      .speaker,
      .silence,
      .volume-up,
      .volume-down,
      .sleep,
      .proximity {
        background-color: #525c6b;
      }

      .home,
      .speaker,
      .camera,
      .proximity {
        left: 50%;
      }

      .home,
      .speaker {
        margin-left: -15px;
        width: 35px;
      }

      .home,
      .camera {
        border: 2px solid #525c6b;
      }

      .home {
        height: 35px;
        bottom: 10px;
      }

      .speaker {
        top: 35px;
        height: 4px;
        border-radius: 5px;
      }

      .camera {
        width: 7px;
        height: 7px;
        top: 33px;
        margin-left: -35px;
      }

      .proximity {
        width: 5px;
        height: 5px;
        top: 10px;
      }

      .silence,
      .volume-up,
      .volume-down,
      .sleep {
        width: 5px;
      }

      .volume-up,
      .volume-down,
      .sleep {
        height: 20px;
      }

      .silence,
      .volume-up,
      .volume-down {
        left: -0.75vh;
      }

      .silence {
        height: 20px;
        top: 30px;
      }

      .volume-up,
      .sleep {
        top: 135px;
      }

      .volume-down {
        top: 90px;
      }

      .sleep {
        right: -5px;
      }

      .footer {
        margin: 25px;
        align-items: center;
        justify-content: center;
        flex-direction: column;
      }

      #unlocker {
        color: red;
      }

      /* Fin de CSS Contenedor */

      h1 {
        font-size: 20px;
        margin: 25px 0;
        color: black;
      }
      @keyframes app-logo-spin {
        from {
          transform: rotate(0deg);
        }
        to {
          transform: rotate(360deg);
        }
      }

      footer {
        margin: 25px;
        color: black;
      }

      .app-footer {
        font-size: calc(12px + 0.5vmin);
        align-items: center;
      }
    `;
  }

  constructor() {
    super();
    this.title = "Final Project, by P. March";
    this.page = "lockscreen";
    this.applications = [
      {
        name: "Appstore",
        notifications: 1,
        image: "/src/img/Appstore.svg",
      },
      {
        name: "Calculator",
        notifications: 0,
        image: "/src/img/Calculator.svg",
      },
      {
        name: "Calendar",
        notifications: 0,
        image: "/src/img/Calendar.svg",
      },
      {
        name: "Camera",
        notifications: 0,
        image: "/src/img/Camera.svg",
      },
      {
        name: "Health",
        notifications: 0,
        image: "/src/img/Health.svg",
      },
      {
        name: "Home",
        notifications: 0,
        image: "/src/img/Home.svg",
      },
      {
        name: "Messages",
        notifications: 10,
        image: "/src/img/Messages.svg",
      },
      {
        name: "Voice",
        notifications: 0,
        image: "/src/img/Voice.svg",
      },
      {
        name: "Notes",
        notifications: 0,
        image: "/src/img/Notes.svg",
      },
      {
        name: "Phone",
        notifications: 3,
        image: "/src/img/Phone.svg",
      },
      {
        name: "Photos",
        notifications: 0,
        image: "/src/img/Photos.svg",
        url: "photos",
      },
      {
        name: "Reminders",
        notifications: 0,
        image: "/src/img/Reminders.svg",
      },
      {
        name: "Settings",
        notifications: 0,
        image: "/src/img/Settings.svg",
      },
      {
        name: "Weather",
        notifications: 0,
        image: "/src/img/Weather.svg",
      },
      {
        name: "Airdrop",
        notifications: 0,
        image: "/src/img/Airdrop.svg",
      },
      {
        name: "Podcasts",
        notifications: 0,
        image: "/src/img/Podcasts.svg",
      },
      {
        name: "Apple TV",
        notifications: 0,
        image: "/src/img/AppleTV.svg",
      },
      {
        name: "Contacts",
        notifications: 0,
        image: "/src/img/Contacts.svg",
      },
      {
        name: "Maps",
        notifications: 0,
        image: "/src/img/Maps.svg",
      },
      {
        name: "Wallet",
        notifications: 0,
        image: "/src/img/Wallet.svg",
      },
    ];
    this.contacts = [
      {
        avatar:
          "https://cdn.icon-icons.com/icons2/2201/PNG/512/telegram_logo_circle_icon_134012.png",
        name: "Telegram",
        lastMessage: {
          text: "Tienes una actualización",
          time: `${new Date().getHours()}:${new Date().getMinutes()}`,
        },
        messages: [
          {
            seen: false,
          },
        ],
      },
      {
        avatar:
          "https://cdn.icon-icons.com/icons2/2201/PNG/512/telegram_logo_circle_icon_134012.png",
        name: "Telegram",
        lastMessage: {
          text: "Tienes una actualización",
          time: `${new Date().getHours()}:${new Date().getMinutes()}`,
        },
        messages: [
          {
            seen: false,
          },
        ],
      },
      {
        avatar:
          "https://cdn.icon-icons.com/icons2/2201/PNG/512/telegram_logo_circle_icon_134012.png",
        name: "Telegram",
        lastMessage: {
          text: "Tienes una actualización",
          time: `${new Date().getHours()}:${new Date().getMinutes()}`,
        },
        messages: [
          {
            seen: false,
          },
        ],
      },
    ];

    this.songs = [
      {
        id: 1,
        title: "1904",
        artist: "The Tallest Man on Earth",
        year: "2012",
        img_url: "https://m.media-amazon.com/images/I/91VNoX0DwNL._SL1500_.jpg",
        mp3: "./assets/musicOne.mp3",
      },
      {
        id: 2,
        title: "#40",
        artist: "Dave Matthews",
        year: "1999",
        img_url: "https://m.media-amazon.com/images/I/61OjocsZ0fL.jpg",
        mp3: "./assets/musicTwo.mp3",
      },
      {
        id: 3,
        title: "40oz to Freedom",
        artist: "Sublime",
        year: "1996",
        img_url: "https://m.media-amazon.com/images/I/61oQjWU1JGL.jpg",
        mp3: "./assets/musicOne.mp3",
      },
    ];
  }

  gotoPage(page) {
    this.page = page;
  }

  onApplicationSelected(event) {
    const app = event.detail;
    const components = document.querySelectorAll("app-ios");
    // this.page = app.name;
    for (const component of components) {
      component.applications = component.applications.map((application) => {
        if (application.name === app.name) {
          application.notifications -= 1;
          console.log(application);
        }

        if (app.name === "Messages") {
          this.gotoPage("telegram");
        }
        if (app.name === "Apple TV") {
          this.gotoPage("streaming");
        }
        if (app.name === "Podcasts") {
          this.gotoPage("spotify");
        }
        if (app.name === "Photos") {
          this.gotoPage("photos");
        }
        return application;
      });
    }
    console.log(app.url);
  }

  backHome(page) {
    this.page = page;
  }

  renderFooter() {
    return html`
      <footer>
        <p class="app-footer">
          🧑‍💻 Final Project by
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://github.com/paumrch"
            >Pau March</a
          >.
        </p>
      </footer>
    `;
  }

  toggleUnlock() {
    this.isUnlock = !this.isUnlock;
  }

  unlockScreen = (event) => {
    const unlock = event.detail;
    this.gotoPage("home");
  };

  handlePageView() {
    switch (this.page) {
      case "photos":
        return html` <photos-component .page=${this.page}></photos-component>
          <button
            @click="${() => {
              this.gotoPage("home");
            }}"
          >
            Unlock
          </button>`;
        break;
      case "spotify":
        return html` <spotify-component
            .page=${this.page}
            .list=${this.songs}
          ></spotify-component>
          <button
            @click="${() => {
              this.gotoPage("home");
            }}"
          >
            Back Home
          </button>`;
        break;
      case "telegram":
        return html` <telegram-app></telegram-app>
          <button
            @click="${() => {
              this.gotoPage("home");
            }}"
          >
            Back Home
          </button>`;
        break;
      case "streaming":
        return html` <streaming-app
            .page=${this.page}
            .list=${this.songs}
          ></streaming-app>
          <button
            @click="${() => {
              this.gotoPage("home");
            }}"
          >
            Unlock
          </button>`;
        break;
      case "home":
        return html`
          <apple-component
            .applications=${this.applications}
            @application-selected="${this.onApplicationSelected}"
          ></apple-component>
          <!-- <button
            @click="${() => {
            this.gotoPage("spotify");
          }}"
          >
            Open Spotify
          </button> -->
          <button
            @click="${() => {
              this.gotoPage("lockscreen");
            }}"
          >
            Lock Phone
          </button>
        `;
        break;
      default:
        "lockscreen";
        return html`
          <lock-screen
            .applications=${this.applications}
            @application-selected="${this.onApplicationSelected}"
            @unlock-screen="${this.unlockScreen}"
          ></lock-screen>
          <button
            @click="${() => {
              this.gotoPage("home");
            }}"
          >
            Unlock Phone
          </button>
        `;
        break;
    }
  }

  render() {
    return html`
      <h1>${this.title}</h1>

      <div class="iphone">
        <span class="_button speaker" title="Speaker"></span>
        <span class="_button camera" title="Camera"></span>
        <span class="_button proximity" title="Proximity Sensor"></span>
        <button class="_button silence" title="Silence Switch"></button>
        <button class="_button volume-up" title="Volume Up"></button>
        <button class="_button volume-down" title="Volume Down"></button>
        <button class="_button sleep" title="Sleep"></button>
        <div class="screen">
          <main>${this.handlePageView()}</main>
        </div>
      </div>

      ${this.renderFooter()}
    `;
  }
}
