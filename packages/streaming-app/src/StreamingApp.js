import { LitElement, html, css } from "lit";
import "@minsait/login-component";
import "@minsait/register-component";
import "@minsait/category-component";
import "@minsait/home-component";
import "@minsait/movie-component";
import "@minsait/mylist-component";

import "@material/web/textfield/outlined-text-field";

const logo = new URL("../assets/open-wc-logo.svg", import.meta.url).href;

export class StreamingApp extends LitElement {
  static get properties() {
    return {
      title: { type: String },
      page: { type: String },
    };
  }

  static get styles() {
    return css`
      :host {
        /* background-color: #f4f4f4;
        min-height: 100vh; */
        display: flex;
        flex-direction: column;
        font-size: calc(10px + 2vmin);
        margin: 0 auto;
        text-align: center;
        /* overflow: scroll; */
      }


      header .logo {
        margin: 25px auto;
        max-width: 100px;
        display: block;
      }

      footer {
        background-color: #f4f4f4;
        font-size: calc(10px + 0.75vmin);
      }
    `;
  }

  constructor() {
    super();
    this.title = "My app";
    this.page = "login";
  }

  gotoPage(e) {
    this.page = e.detail;
  }

  backHome(e) {
    this.page = e.detail;
  }

  // API

  async newAccount(event) {
    console.log(event.detail);

    try {
      const response = await fetch(
        "https://centraal.sfi.digital/api/v1/account",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            name: event.detail.name,
            username: event.detail.username,
            email: event.detail.email,
            password: event.detail.password,
          }),
        }
      );

      if (response.status === 200) {
        alert("Usuario registrado con éxito");
        // this._toLogin();
      }
    } catch (error) {
      console.log(error);
    }
  }

  async loggedUser(event) {
    try {
      const response = await fetch(
        "https://centraal.sfi.digital/api/v1/login",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            username: event.detail.username,
            password: event.detail.password,
          }),
        }
      );

      if (response.status === 200) {
        console.log("Sesión iniciada con éxito");
        const payload = await response.json();
        if (payload.success) {
          this.page = "home";
          sessionStorage.setItem("token", payload.data);
          sessionStorage.setItem("account", payload.account);
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  _gotoMovie(event) {
    this.selectedMovie = event.detail;

    this.page = "movie";

    console.log("click");
  }

  // Fin de API

  render() {
    return html`
      <header>
        <div class="logo">
          <img src="../assets/stream-naming-black.svg" alt="" />
        </div>
      </header>
      <main>${this.handlePageView()}</main>
    `;
  }

  handlePageView() {
    switch (this.page) {
      case "register":
        return html`
          <register-component
            @page-view=${this.gotoPage}
            @new-account=${this.newAccount}
          ></register-component>
        `;
        break;
      case "movie":
        return html`
          <movie-component
            .selectedMovie="${this.selectedMovie}"
            @page-view=${this.gotoPage}
            @back-home=${this.backHome}
          ></movie-component>
        `;
        break;
      case "home":
        return html`
          <home-component
            @page-view=${this.gotoPage}
            @new-account=${this.newAccount}
            @movie-selected=${this._gotoMovie}
          ></home-component>
        `;
        break;
      // default:
      //   "home";
      //   return html`
      //    <home-component
      //     @page-view=${this.gotoPage}
      //     @new-account=${this.newAccount}
      //     @movie-selected=${this._gotoMovie}
      //   ></home-component>
      //   `;
      //   break;
      default:
        "login";
        return html`
          <login-component
            @page-view=${this.gotoPage}
            @user-login=${this.loggedUser}
          ></login-component>
        `;
        break;
    }
  }
}
