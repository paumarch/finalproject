import { html, css, LitElement } from "lit";

export class PlayerComponent extends LitElement {
  static get styles() {
    return css`
      :host {
        border: 1px solid;
        display: block;
        color: var(--player-component-text-color, #000);
        padding: 10px;
      }
      .player__controls {
        margin: 15px 0 0 0;
      }
    `;
  }

  static get properties() {
    return {
      songList: { Object },
    };
  }

  constructor() {
    super();
  }

  render() {
    return html`
      <h1>Player Component</h1>
      <div class="player">
        <div>
          <p>Ahora está sonando...</p>
          <h3>Title</h3>
          <span>
            <p>De...</p>
            <p>Artist</p>
          </span>
        </div>
      <audio src="" controls>
        <p>
          If you are reading this, it is because your browser does not support
          the audio element.
        </p>
      </audio>
      </div>
      <div class="player__controls">
        <button>Prev</button>
        <button>Play</button>
        <button>Pause</button>
        <button>Next</button>
      </div>
    `;
  }
}
