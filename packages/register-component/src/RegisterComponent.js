import { html, css, LitElement } from "lit";
import "@material/web/textfield/outlined-text-field";

export class RegisterComponent extends LitElement {
  static get styles() {
    return css`
      @import url("https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,700;1,400;1,700&display=swap");
      :host {
        display: flex;
        flex-direction: column;
        margin: auto;
        color: var(--login-component-text-color, #000);
        font-family: "Roboto", sans-serif;
      }

      img {
        margin-top: 25px;
        object-fit: cover;
        min-height: 40vh;
      }

      .form {
        margin: 5px auto;
      }

      p {
        font-size: calc(10px + 0.75vmin);
      }

      a {
        text-decoration: underline;
      }
    `;
  }

  static get properties() {
    return {
      title: { type: String },
    };
  }

  constructor() {
    super();
    this.title = "Regístrate";
  }

  gotoLogin() {
    this.page = "login";
    const customEvent = new CustomEvent("page-view", {
      detail: this.page,
    });
    this.dispatchEvent(customEvent);
  }

  async userRegister(event) {
    event.preventDefault();

    const name = this.shadowRoot.querySelector("#name");
    const username = this.shadowRoot.querySelector("#user");
    const email = this.shadowRoot.querySelector("#email");
    const password = this.shadowRoot.querySelector("#password");

    this.dispatchEvent(
      new CustomEvent("new-account", {
        detail: {
          name: name.value,
          username: username.value,
          email: email.value,
          password: password.value,
        },
      })
    );
  }

  render() {
    return html`
      <section>
        <h2>${this.title}</h2>
        <div class="container">
          <md-outlined-text-field
            class="form"
            id="name"
            name="Nombre"
            label="Nombre"
          >
          </md-outlined-text-field>
          <md-outlined-text-field
            class="form"
            id="surname"
            name="Apellidos"
            label="Apellidos"
          ></md-outlined-text-field>
        </div>
        <div class="container">
          <md-outlined-text-field
            class="form"
            id="user"
            name="Usuario"
            label="Usuario"
          >
          </md-outlined-text-field>
          <md-outlined-text-field
            class="form"
            id="password"
            type="password"
            name="Password"
            label="Contraseña"
          ></md-outlined-text-field>
        </div>
        <div class="container">
          <md-outlined-text-field
            class="form"
            id="email"
            type="email"
            name="Email"
            label="Email"
          ></md-outlined-text-field>
        </div>
        <div class="container">
          <md-text-button
            @click="${this.userRegister}"
            class="button"
            id="submit"
            label="Registrar"
          ></md-text-button>
          <p>
            ¿Ya eres usuario?
            <a @click="${this.gotoLogin}">Accede a tu cuenta</a>.
          </p>
        </div>
        <div>
          <img
            src="../assets/movies_background.jpeg"
            alt="Streaming App Image Background"
            width="100%"
          />
        </div>
      </section>
    `;
  }
}
