import { LitElement, html, css } from "lit";

import "@minsait/telegram-app";
import "@minsait/streaming-app";
import "@minsait/spotify-app";
import "@minsait/app-ios";

export class PhoneApp extends LitElement {
  static get properties() {
    return {
      title: { type: String },
      page: { type: String },
    };
  }

  static get styles() {
    return css`
      :host {
        min-height: 100vh;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: flex-start;
        font-size: calc(10px + 2vmin);
        color: #1a2b42;
        max-width: 960px;
        margin: 0 auto;
        text-align: center;
        background-color: var(--phone-app-background-color);
      }

      main {
        flex-grow: 1;
      }

      .logo {
        margin-top: 36px;
        animation: app-logo-spin infinite 20s linear;
      }

      @keyframes app-logo-spin {
        from {
          transform: rotate(0deg);
        }
        to {
          transform: rotate(360deg);
        }
      }

      .app-footer {
        font-size: calc(12px + 0.5vmin);
        align-items: center;
      }

      .app-footer a {
        margin-left: 5px;
      }
    `;
  }

  constructor() {
    super();
    this.title = "Phone App";
    this.page = "phoneApp";
  }

  gotoPage(page) {
    this.page = page;
  }

  handlePageView() {
    switch (this.page) {
      case "spotify":
        return html` <spotify-component .page=${this.page}></spotify-component>
          <button
            @click="${() => {
              this.gotoPage("home");
            }}"
          >
            Unlock
          </button>`;
        break;
      case "home":
        return html` <apple-component
            .applications=${this.applications}
            @application-selected="${onApplicationSelected}"
          ></apple-component>
          <button
            @click="${() => {
              this.gotoPage("spotify");
            }}"
          >
            Open Spotify
          </button>
          <button
            @click="${() => {
              this.gotoPage("lockscreen");
            }}"
          >
            Lock Phone
          </button>
          <button
            @click="${() => {
              this.gotoPage("spotify");
            }}"
          >
            Open Spotify
          </button>
          <button
            @click="${() => {
              this.gotoPage("photos");
            }}"
          >
            Abrir Photos
          </button>`;
        break;
      default:
        "phoneApp";
        return html` <app-ios></app-ios>
          <button
            @click="${() => {
              this.gotoPage("home");
            }}"
          >
            Unlock Phone
          </button>
          <button
            @click="${() => {
              this.gotoPage("spotify");
            }}"
          >
            Open Spotify
          </button>
          <button
            @click="${() => {
              this.gotoPage("photos");
            }}"
          >
            Abrir Photos
          </button>`;
        break;
    }
  }

  render() {
    return html`

<h1>${this.title}</h1>
        ${this.handlePageView()}
      </main>

    `;
  }
}
