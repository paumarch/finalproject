import { html, css, LitElement } from "lit";
import "../../player-component/player-component";
import "@vaadin/vaadin-icon";

export class SpotifyComponent extends LitElement {
  static get styles() {
    return css`
      .spotifyContainer {

        color: #ffffff;

        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto,
          Oxygen, Ubuntu, Cantarell, "Open Sans", "Helvetica Neue", sans-serif;
        
      }
      .song__container {
        display: flex;
    flex-direction: row-reverse;
    place-content: center;
    margin: 5px;
    padding: 0px 5px;
    align-items: center;
    width: 300px;
    border-radius: 20px;
    background-color: rgba(255, 255, 255, 0.19);
    gap: 5px;
    justify-content: space-between;
}

.song__body {
        display: flex;
        gap: 10px;
      }

      .song__body h3 {
        font-size: 10px;
      }
      .player {
    display: flex;
    justify-content: center;
    flex-direction: column;
    align-items: center;
}
      .player img {
        max-width: 75px;
      }

      .player h3 {
        font-size: 14px;
      }

      audio {
        max-width: 250px;
        max-height: 25px;
      }

      :host {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: flex-start;
        font-size: calc(10px + 2vmin);
        color: #1a2b42;
        margin: 0 auto;
        text-align: center;
        background-color: var(--app-ios-background-color);
      }

      p {
        font-size: 10px;
      }

      ._button {
        border: none;
        padding: 0;
        outline: none;
        background-color: transparent;
        user-select: none;
      }

      span {
        box-sizing: border-box;
      }

      .header {
        display: block;
        justify-content: center;
        padding: 15px;
        /* width: 412px; */
      }

      .iron-icon {
        color: #ffffff;
        max-height: 14px;
      }

      .header .left {
        float: left;
        font-weight: bold;
        color: #ffffff;
      }

      .header .right {
        float: right;
      }

      .app {
        display: block;
        text-align: center;
      }

      .app img {
        border-radius: 5px;
        width: 40px;
        height: 40px;
        margin: 5px;
      }

      .name {
        margin-top: -10px;
        color: #ffffff;
      }

      .name p {
        font-size: 10px;
      }

      .badge {
        position: absolute;
        background: -webkit-radial-gradient(
            center -9px,
            circle cover,
            white 0px,
            red 26px
          )
          red;
        border: 1px solid white;
        border-radius: 50%;
        box-shadow: black 1px 1px 1px;
        color: white;
        font: bold 17px / 13px Helvetica, Verdana, Tahoma;
        height: 10px;
        /* padding-top: 0; */
        padding-bottom: 2px;
        text-align: center;
        min-width: 13px;
        font-size: 8px;
      }

      .icon:active {
        opacity: 0.75;
      }

      .icon:nth-last-of-type(-n + 4) {
        margin-top: 1.5vh;
      }

      .iphone {
        padding: 60px 10px;
        border-radius: 25px;
        position: relative;
      }

      .home,
      .camera,
      .proximity,
      .speaker,
      .silence,
      .volume-up,
      .volume-down,
      .sleep {
        position: absolute;
      }

      .home,
      .camera,
      .proximity {
        border-radius: 50%;
      }

      .speaker,
      .silence,
      .volume-up,
      .volume-down,
      .sleep,
      .proximity {
        background-color: #525c6b;
      }

      .home,
      .speaker,
      .camera,
      .proximity {
        left: 50%;
      }

      .home,
      .speaker {
        margin-left: -15px;
        width: 35px;
      }

      .home,
      .camera {
        border: 2px solid #525c6b;
      }

      .home {
        height: 35px;
        bottom: 10px;
      }

      .speaker {
        top: 35px;
        height: 4px;
        border-radius: 5px;
      }

      .camera {
        width: 7px;
        height: 7px;
        top: 33px;
        margin-left: -35px;
      }

      .proximity {
        width: 5px;
        height: 5px;
        top: 10px;
      }

      .silence,
      .volume-up,
      .volume-down,
      .sleep {
        width: 5px;
      }

      .volume-up,
      .volume-down,
      .sleep {
        height: 20px;
      }

      .silence,
      .volume-up,
      .volume-down {
        left: -0.75vh;
      }

      .silence {
        height: 20px;
        top: 30px;
      }

      .volume-up,
      .sleep {
        top: 135px;
      }

      .volume-down {
        top: 90px;
      }

      .sleep {
        right: -5px;
      }

      .footer {
        margin: 25px;
        align-items: center;
        justify-content: center;
        flex-direction: column;
      }

      .appTitle {
        font-size: 1.5rem;
      }
    `;
  }

  static get properties() {
    return {
      page: { type: String },
      list: { type: Array },
      songList: { type: Array },
      query: { type: String },
      songtoListen: { type: Object },
      openPlayer: { type: Boolean },
      songAutoplay: { type: Boolean },
    };
  }

  constructor() {
    super();
    this.page = "spotify";
    this.list = [];
    this.songList = [];
    this.query = "";
    this.songtoListen = {};
    this.openPlayer = false;
    this.songAutoplay = true;
  }

  onFilter(event) {
    const query = event.currentTarget.value;
    this.query = query;
    this.songList = this.list.filter((song) =>
      song.title.toLowerCase().trim().includes(query.toLowerCase().trim())
    );
  }

  firstUpdated(_changedProperties) {
    super.firstUpdated(_changedProperties);
    this.songList = [...this.list];
  }

  songSelected(toListen) {
    this.songtoListen = toListen;
    this.openPlayer = true;
  }

  render() {
    return html`

      <div class="spotifyContainer">
       
            
      <div>
    <input
      type="text"
      placeholder="¿Qué quieres escuchar?"
      @input="${this.onFilter}"
    />
    ${this.songList.map(
      (song) => html`
        <div class="song__container">
          <div class="song__header">
            <button
              @click="${() => {
                this.songSelected(song);
              }}"
            >
              Listen
            </button>
          </div>
          <div class="song__body">
            <h3>${song.title}</h3>
            <p>${song.artist} - ${song.year}</p>
          </div>
        </div>
      `
    )}
    ${
      this.openPlayer === true
        ? html`
            <div class="player">
              <div>
                <p>Ahora está sonando...</p>
                <img src=${this.songtoListen.img_url} />
                <h3>${this.songtoListen.title}</h3>
                <span>
                  <p>De: ${this.songtoListen.artist}</p>
                </span>
              </div>
              ${this.songAutoplay === true
                ? html`
                    <audio src="${this.songtoListen.mp3}" controls autoplay>
                      <p>
                        If you are reading this, it is because your browser does
                        not support the audio element.
                      </p>
                    </audio>
                  `
                : html`
                    <audio src="${this.songtoListen.mp3}" controls>
                      <p>
                        If you are reading this, it is because your browser does
                        not support the audio element.
                      </p>
                    </audio>
                  `}
            </div>
            <div class="player__controls">
              <button
                @click="${() => {
                  this.nextSong();
                }}"
              >
                Next
              </button>
              <button
                @click="${() => {
                  this.prevSong();
                }}"
              >
                Prev
              </button>
              <button
                @click="${() => {
                  this.playSong();
                }}"
              >
                Play
              </button>
              <button
                @click="${() => {
                  this.stopSong();
                }}"
              >
                Stop
              </button>

              <button
                @click="${() => {
                  this.closePlayer();
                }}"
              >
                Close Player
              </button>
            </div>
          `
        : html``
    }
  </div>
          </div>
        </div>
      
      </div>

    `;
  }

  nextSong(e) {
    let element;
    e = this.songtoListen.id;

    if (e + 1 > this.list.length) {
      element = 1;
    } else {
      element = e + 1;
    }
    this.songtoListen = this.list.find((song) => song.id === element);
  }

  prevSong(e) {
    let element;
    e = this.songtoListen.id;

    if (e - 1 <= 0) {
      element = this.list.length;
    } else {
      element = e - 1;
    }

    this.songtoListen = this.list.find((song) => song.id === element);
  }

  closePlayer() {
    this.openPlayer = !this.openPlayer;
  }

  stopSong() {
    this.songAutoplay = !this.songAutoplay;
  }

  playSong() {
    this.songAutoplay = !this.songAutoplay;
  }
}
