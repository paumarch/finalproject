import { html, css, LitElement } from "lit";
import "@minsait/category-component";
import "@material/web/textfield/outlined-text-field";

import "@vaadin/text-field";
import "@vaadin/button";

export class HomeComponent extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        padding: 25px;
        color: var(--category-component-text-color, #fff);
        font-size: 10px;
      }
      .container {
        background-color: #313638;
        display: flex;
        flex-direction: column;
        /* padding-left: 30px; */
        /* padding-right: 30px; */
      }
      .row {
        display: flex;
        flex-direction: row;
        /* gap: 20px; */
        overflow-x: hidden
        /* height: 300px; */
      }

      .title h3 {
        font-size: 10px;
        color: white;
      }

      .category {
        display: flex;
        flex-direction: column;
        text-align: center;
      }

      .movie {
    display: flex;
    flex-direction: column;
    list-style: none;
    justify-content: center;
    gap: 5px;
    width: 70px;
    align-items: center;
}

      img {
        height: 100px;
        transition: all 0.1s ease-in-out;
        cursor: pointer;
      }

      img:hover {
        transform: scale(1.25);
      }

      ::-webkit-scrollbar {
        width: 10px;
      }

      ::-webkit-scrollbar-track {
        border-radius: 10px;
        background-color: gray;
      }

      ::-webkit-scrollbar-thumb {
        border-radius: 5px;
        background-color: lightgray;
      }
    `;
  }

  static get properties() {
    return {
      page: { type: String },
      title: { type: String },
      movies: { type: Array },
      categories: { type: Array },
      movieList: { type: Array },
    };
  }

  constructor() {
    super();
    this.page = "home"
    this.title = "Category Component";
    this.categories = [];
    this.movies = new Map();
    this.movieList = [];
  }

  async loadMoviesByCategory() {
    for (const category of this.categories) {
      try {
        const response = await fetch(
          `https://centraal.sfi.digital/api/v1/movie?where[category]=${category._id}`,
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        );
        const payload = await response.json();
        this.movies.set(category.title, payload.data);
        this.requestUpdate();
      } catch (error) {
        // Handle error
      }
    }
  }

  async firstUpdated(_changedProperties) {
    super.firstUpdated(_changedProperties);
    const response = await fetch(
      `https://centraal.sfi.digital/api/v1/category?sort[title]=ASC`,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    const payload = await response.json();
    this.categories = [...payload.data];
    await this.loadMoviesByCategory();
  }

  async onMovieSelect(movie, category) {
    this.dispatchEvent(
      new CustomEvent("movie-selected", {
        detail: {
          movie: movie,
          category: category,
        },
      })
    );
  }

  gotoPage() {
    this.page = "movie";
    const customEvent = new CustomEvent("page-view", {
      detail: this.page,
    });
    this.dispatchEvent(customEvent);
  }

  async onMovieSelect(movie) {
    this.dispatchEvent(
      new CustomEvent("movie-selected", {
        detail: movie,
      })
    );
    console.log(movie);
  }

  _gotoMovie(event) {
    this.selectedMovie = event.detail;
    this.page = "movie";
    console.log(this.page);
  }


  render() {
    return html`
      ${this.categories.map(
        (category) => html`
          <div class="category">
            <h3 class="title">${category.title}</h3>
            <div class="row">
              ${this.movies && this.movies.size > 0
                ? this.movies.get(category.title).map(
                    (movie) => html`
                      <div class="movie"
                      @click="${() => {
                        this.onMovieSelect(movie);
                      }}">
                        <img src="${movie.cover}">
                      </div>
            </div>
                    `
                  )
                : ""}
            </div>
          </div>
        `
      )}
    `;
  }

  handlePageView() {
    switch (this.page) {
      case "movie":
        return html`
          <movie-component
            .selectedMovie="${this.selectedMovie}"
            @page-view=${this.gotoPage}
            @back-home=${this.backHome}
          ></movie-component>
        `;
        break;
      default:
        "home";
        return html`
         <home-component
          @page-view=${this.gotoPage}
          @new-account=${this.newAccount}
          @movie-selected=${this._gotoMovie}
        ></home-component>
        `;
        break;
    }
  }
}