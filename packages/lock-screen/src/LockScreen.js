import { html, css, LitElement } from "lit";

export class LockScreen extends LitElement {
  static get styles() {
    return css`
      :host {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: flex-start;
        font-size: calc(10px + 2vmin);
        color: #1a2b42;
        max-width: 960px;
        margin: 0 auto;
        text-align: center;
        background-color: var(--app-ios-background-color);
        color: #ffffff;
      }

      p {
        font-size: 10px;
      }

      .container {
        display: flex;
        width: 400px;
        height: 915px;
      }
      ._button {
        border: none;
        padding: 0;
        outline: none;
        background-color: transparent;
        user-select: none;
      }

      span {
        box-sizing: border-box;
      }

      .screen {
        background-color: #4158d0;
        background-image: linear-gradient(
          43deg,
          #4158d0 0%,
          #c850c0 46%,
          #ffcc70 100%
        );

        display: flex;
        flex-wrap: wrap;
        justify-content: center;
        padding: 15px;
        max-width: 320px;
        height: 400px;
        max-height: 495px;
      }

      .header {
        display: block;
        justify-content: center;
        width: 300px;
      }

      .iron-icon {
        color: #ffffff;
        max-height: 14px;
      }

      .header .left {
        display: flex;
        float: left;
        font-size: 8px;
        color: rgb(255, 255, 255);
        align-items: center;
      }

      .header .right {
        float: right;
      }

      .app {
        display: block;
        text-align: center;
      }

      .app img {
        border-radius: 5px;
        width: 40px;
        height: 40px;
        margin: 5px;
      }

      .notification {
        display: flex;
        background-color: #ffffff7b;
        padding: 7px 20px 0 5px;
        margin-top: 5px;
        border:1px solid none;
        border-radius: 10px;
      }

      .notification img {
        border-radius: 5px;
        width: 40px;
        height: 40px;
        margin: 5px;
      }

      .dayHour {
        margin-top: 50px;
        display: flex;
        flex-direction: column;
      }
      .timer p {
        font-size: 24px;
        font-weight: bold;
        margin: 0;
      }

      .date p {
        font-size: 12px;
      }

      .name {
        margin-top: -10px;
        color: #ffffff;
      }

      .name p {
        font-size: 10px;
      }

      .badge {
        position: absolute;
        background: -webkit-radial-gradient(
            center -9px,
            circle cover,
            white 0px,
            red 26px
          )
          red;
        border: 1px solid white;
        border-radius: 50%;
        box-shadow: black 1px 1px 1px;
        color: white;
        font: bold 17px / 13px Helvetica, Verdana, Tahoma;
        height: 10px;
        /* padding-top: 0; */
        padding-bottom: 2px;
        text-align: center;
        min-width: 13px;
        font-size: 8px;
      }

      .badge__lockscreen {
        position: absolute;
        background: -webkit-radial-gradient(
            center -9px,
            circle cover,
            white 0px,
            red 26px
          )
          red;
        border: 1px solid white;
        border-radius: 50%;
        box-shadow: black 1px 1px 1px;
        color: white;
        font: bold 17px / 13px Helvetica, Verdana, Tahoma;
        height: 13px;
        /* padding-top: 0; */
        padding-bottom: 2px;
        text-align: center;
        min-width: 15px;
        font-size: 8px;
      }

      .icon:active {
        opacity: 0.75;
      }

      .icon:nth-last-of-type(-n + 4) {
        margin-top: 1.5vh;
      }

      .iphone,
      .screen {
        border: 2px solid #525c6b;
      }

      .iphone {
        padding: 60px 10px;
        border-radius: 25px;
        position: relative;
      }

      .home,
      .camera,
      .proximity,
      .speaker,
      .silence,
      .volume-up,
      .volume-down,
      .sleep {
        position: absolute;
      }

      .home,
      .camera,
      .proximity {
        border-radius: 50%;
      }

      .speaker,
      .silence,
      .volume-up,
      .volume-down,
      .sleep,
      .proximity {
        background-color: #525c6b;
      }

      .home,
      .speaker,
      .camera,
      .proximity {
        left: 50%;
      }

      .home,
      .speaker {
        margin-left: -15px;
        width: 35px;
      }

      .home,
      .camera {
        border: 2px solid #525c6b;
      }

      .home {
        height: 35px;
        bottom: 10px;
      }

      .speaker {
        top: 35px;
        height: 4px;
        border-radius: 5px;
      }

      .camera {
        width: 7px;
        height: 7px;
        top: 33px;
        margin-left: -35px;
      }

      .proximity {
        width: 5px;
        height: 5px;
        top: 10px;
      }

      .silence,
      .volume-up,
      .volume-down,
      .sleep {
        width: 5px;
      }

      .volume-up,
      .volume-down,
      .sleep {
        height: 20px;
      }

      .silence,
      .volume-up,
      .volume-down {
        left: -0.75vh;
      }

      .silence {
        height: 20px;
        top: 30px;
      }

      .volume-up,
      .sleep {
        top: 135px;
      }

      .volume-down {
        top: 90px;
      }

      .sleep {
        right: -5px;
      }

      .footer {
        margin: 25px;
        align-items: center;
        justify-content: center;
        flex-direction: column;
      }

      #unlocker {
        color: red;
      }
    `;
  }

  static get properties() {
    return {
      page: {
        type: String,
      },
      name: {
        type: String,
      },
      image: {
        type: String,
      },
      hasNotifications: {
        type: Boolean,
      },
      notifications: {
        type: Number,
      },
      showApplications: {
        type: Boolean,
      },
      applications: {
        type: Array,
      },
      isCharging: {
        type: Boolean,
      },
      time: {
        type: String,
      },
      wifiOn: {
        type: Boolean,
      },
    };
  }

  constructor() {
    super();
    this.page = "lock";
    this.name = "";
    this.image = "";
    this.hasNotifications = true;
    this.notifications = "";
    this.applications = [];
    this.isCharging = false;
    this.time = new Date();
    this.date = new Date();
    this.wifiOn = false;
  }

  gotoPage(page) {
    this.page = page;
  }

  chargingPhone() {
    this.isCharging = !this.isCharging;
  }

  wifiOnOff() {
    this.wifiOn = !this.wifiOn;
  }

  onApplicationClick(event) {
    const application = event.currentTarget.application;
    console.log('Click sobre elemento', application)
    const customEvent = new CustomEvent('application-selected', {
      detail: application
    })
    this.dispatchEvent(customEvent);
  }

  onHomeButtonClick(event) {
    const button = event.currentTarget.title;
    console.log('Click sobre elemento', button)
    const customEvent = new CustomEvent('button-selected', {
      detail: button
    })
    this.dispatchEvent(customEvent);
  }

  firstUpdated(){
    const unlocker = this.shadowRoot.getElementById('unlocker');
    unlocker.addEventListener('touchstart', this.touchStart);
    unlocker.addEventListener('touchmove', this.touchMove);
    unlocker.addEventListener('touchend', this.touchEnd);
  }

  touchStart(){
    console.log("Start");
  }

  touchMove(){
    console.log("Move");    
  }

  touchEnd(event){
    console.log("End");
    const touchButton = event.currentTarget.touchButton;
    const customEvent = new CustomEvent("unlock-screen", {
      detail: touchButton,
    });
    this.dispatchEvent(customEvent);
  
  }

  render() {
    return html`
    <main>
          <div class="header">
            <div class="left">
              <p>Movistar</p>
              <iron-icon
                class="iron-icon"
                icon="device:signal-cellular-3-bar"
              ></iron-icon>
            </div>
            <div class="right">
              ${this.isCharging
                ? html`
                    <iron-icon
                      class="iron-icon"
                      icon="device:battery-charging-full"
                    ></iron-icon>
                  `
                : html`
                    <iron-icon
                      class="iron-icon"
                      icon="device:battery-50"
                    ></iron-icon>
                  `}
              ${this.wifiOn
                ? html`
                    <iron-icon
                      class="iron-icon"
                      icon="device:signal-wifi-4-bar"
                    ></iron-icon>
                  `
                : html`
                    <iron-icon
                      class="iron-icon"
                      icon="device:signal-wifi-off"
                    ></iron-icon>
                  `}
            </div>

            <div class="dayHour">
              <span class="timer">
                <p>
                  ${`${this.time.getHours()} : ${
                    this.time.getMinutes() < 10 ? "0" : ""
                  }${this.time.getMinutes()}`}
                </p>
              </span>
              <span class="date">
                <p>${`${this.date.toDateString()}`}</p>
              </span>
            </div>
          </div>
          <div>
            ${this.applications.map(
              (application) => html`
               
                  ${application.notifications !== 0
                  ? html`
                   <div class="notification" @click="${this.onApplicationClick}" .application="${application}">
                      <span class="badge__lockscreen">${application.notifications}
                    </span>
                    <img src="${application.image}" />
                  <div>
                    <p>${application.name}</p>
                    <p>Marcar como leída</p>
                  </div>
                  </div>
                    `
                  : ""}
                
              ` 
            )}
          </div>   
          </main>
    `;
  }
}
