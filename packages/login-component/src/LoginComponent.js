import { html, css, LitElement } from "lit";
import "@material/web/textfield/outlined-text-field";
import "@material/web/button/text-button";

export class LoginComponent extends LitElement {
  static get styles() {
    return css`
      @import url("https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,700;1,400;1,700&display=swap");
      :host {
        display: flex;
        flex-direction: column;
        color: var(--login-component-text-color, #000);
        font-family: "Roboto", sans-serif;
      }

      section {
        display: flex;
        align-items: center;
        flex-direction: column;
        min-height: 30vh;
      }

      .container {
        min-height: 35vh;
        margin: auto;
      }

      .img-container img {
        max-height: 50px;
      }

      img {
        margin-top: 5px;
        object-fit: cover;
        /* min-height: 40vh; */
      }

      .form {
        margin: 5px auto;
      }

      p {
        font-size: calc(10px + 0.75vmin);
        color: white;
      }

      a {
        text-decoration: underline;
      }

      h2 {
        font-size:14px;
        color: white;
      }

    `;
  }

  static get properties() {
    return {
      title: { type: String },
    };
  }

  constructor() {
    super();
    this.title = "Accede a tu cuenta";
  }

  gotoRegister() {
    this.page = "register";
    const customEvent = new CustomEvent("page-view", {
      detail: this.page,
    });
    this.dispatchEvent(customEvent);
  }

  async userLogin(event) {
    event.preventDefault();

    const username = this.shadowRoot.querySelector("#user");
    const password = this.shadowRoot.querySelector("#password");

    this.dispatchEvent(
      new CustomEvent("user-login", {
        detail: {
          username: username.value,
          password: password.value,
        },
      })
    );
  }

  render() {
    return html`
      <h2>${this.title}</h2>
      <section>
      <div class="container">
        <div>
        <md-outlined-text-field
          class="form"
          id="user"
          name="Usuario"
          label="Usuario"
        >
        </md-outlined-text-field>
        <md-outlined-text-field
          class="form"
          id="password"
          type="password"
          name="Password"
          label="Contraseña"
        ></md-outlined-text-field>
      </div>
      <div>
        <md-text-button
          @click="${this.userLogin}"
          class="button"
          id="submit"
          label="Acceder"
        ></md-text-button>
        <p>
          ¿Todavía no estás registrado?
          <a @click="${this.gotoRegister}">Regístrate aquí</a>.
        </p>
        </div>
      </div>
      <!-- <div class="img-container">
        <img
          src="../assets/movies_background.jpeg"
          alt="Streaming App Image Background"
          width="100%"
        />
      </div> -->
      </section>
    `;
  }
}
