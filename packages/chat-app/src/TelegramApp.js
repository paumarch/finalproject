import {LitElement, html, css} from 'lit';

import '@minsait/chat-box';
import '@minsait/chat-list';

export class TelegramApp extends LitElement {
  static get properties() {
    return {
      profile: {
        type: Object
      },
      contacts: {
        type: Array
      },
      contactSelected: {
        type: Object
      },
      date: {
        type: String
      },
    };
  }

  static get styles() {
    return css`
      .container {
        display: flex;
        width: 100%;
        /* height: 540px; */
        gap:5px;
      }

      .sidebar {
        background-color: #53baff24;
        border-radius: 10px;
        width: 40%;
      }

      .body {
        background-color: #74ffa9;
        border-radius: 10px;
        width: 60%;
      }
    `;
  }

  constructor() {
    super();
    this.date = new Date();
    this.profile = {
        name: 'Pau March',
        avatar: 'https://randomuser.me/api/portraits/thumb/men/1.jpg',
      };

    this.contacts = [
      {
        name: 'Rafa Nadal',
        avatar: 'https://randomuser.me/api/portraits/thumb/men/75.jpg',
        lastMessage: {
          text: 'Hola! Este es el último mensaje.',
          time: `${`${this.date.getHours()} : ${
            this.date.getMinutes() < 10 ? "0" : ""
          }${this.date.getMinutes()}`}
`
        },
        messages: [
          {
            seen: false,
            text: 'Hola! Este es el primer mensaje',
            owner: {
              name: 'Rafa Nadal',
              avatar: 'https://randomuser.me/api/portraits/thumb/men/75.jpg'
            }
          },
          {
            seen: false,
            text: 'Hola! Este es el segundo mensaje.',
            owner: {
              name: 'Rafa Nadal',
              avatar: 'https://randomuser.me/api/portraits/thumb/men/75.jpg'
            }
          }
        ]
      },
      {
        name: 'Fernando Alonso',
        avatar: 'https://randomuser.me/api/portraits/thumb/men/34.jpg',
        lastMessage: {
          text: 'Hola! Este es el último mensaje.',
          time: `${`${this.date.getHours()} : ${
            this.date.getMinutes() < 10 ? "0" : ""
          }${this.date.getMinutes()}`}
`
        },
        messages: [
          {
            seen: false,
            text: 'Hola! Este es el primer mensaje.',
            owner: {
              name: 'Fernando Alonso',
              avatar: 'https://randomuser.me/api/portraits/thumb/men/34.jpg'
            }
          },
          {
            seen: false,
            text: 'Hola! Este es el segundo mensaje.',
            owner: {
              name: 'Fernando Alonso',
              avatar: 'https://randomuser.me/api/portraits/thumb/men/34.jpg'
            }
          },
          {
            seen: false,
            text: 'Hola! Este es el tercer mensaje.',
            owner: {
              name: 'Fernando Alonso',
              avatar: 'https://randomuser.me/api/portraits/thumb/men/34.jpg'
            }
          }
        ]
      }
    ];
    this.contactSelected = {};
  }

  onContactSelected({detail: contact}) {
    this.contactSelected = contact;
  }

  onTelegramReceived({detail: newTelegram}){
    this.contactSelected.messages.push(newTelegram);
    this.contactSelected = {...this.contactSelected};
  }

  render() {
    return html`
      <main>
        <div class="container">
          <div class="sidebar">
            <chat-list .profile="${this.profile}" .contacts="${this.contacts}" 
            @contact-selected="${this.onContactSelected}"
            ></chat-list>
          </div>
          <div class="body">
            <chat-box .contact="${this.contactSelected}" @telegram-sent="${this.onTelegramReceived}"></chat-box>
          </div>
        </div>
      </main>
    `;
  }
}
