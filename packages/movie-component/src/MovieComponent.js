import { html, css, LitElement } from "lit";

import "@vaadin/icons";
import "@vaadin/button";

export class MovieComponent extends LitElement {
  static get styles() {
    return css`
      :host {
        margin: 25px;
        color: var(--movie-component-text-color, #000);
        scroll-behavior: smooth;
      }

      .container {
    display: flex;
    /* flex-flow: row wrap; */
    align-items: center;
    justify-content: center;
    flex-direction: row;
    gap: 10px;
}

      .player {
        margin: 25px auto;
        min-height: 50vh;
      }

      .controls {
        display: flex;
        gap: 20px;
        flex-direction: column;
      }

      p {
        font-size: calc(10px + 0.75vmin);
        text-align: justify;
      }

      h1 {
        font-size: calc(16px + 0.75vmin);
      }

      h2 {
        font-size: calc(14px + 0.75vmin);
      }

      h3 {
        font-size: calc(12px + 0.75vmin);
      }

      h4 {
        font-size: calc(10px + 0.75vmin);
      }

      a {
        text-decoration: underline;
        font-size: calc(10px + 0.75vmin);
      }

      img {
        max-width: 100px;
      }

      iframe {
        min-width: 100%;
        min-height: 600px;
      }

      .title {
        margin: 0;
      }
    `;
  }

  static get properties() {
    return {
      title: { type: String },
      categories: { type: Array },
      selectedMovie: { type: Object },
      selectedMovieURL: { type: String },
      videoPlayer: { type: Boolean },
    };
  }

  constructor() {
    super();
    this.title = "Movie Component";
    this.categories = [];
    this.selectedMovie = {};
    this.selectedMovieURL = "";
    this.videoPlayer = false;
  }

  backHome() {
    this.page = "home";
    const customEvent = new CustomEvent("back-home", {
      detail: this.page,
    });
    this.dispatchEvent(customEvent);
  }

  loadCategories() {
    fetch("https://centraal.sfi.digital/api/v1/category/")
      .then((response) => response.json())
      .then((payload) => {
        this.category = payload;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  firstUpdated(_changedProperties) {
    super.firstUpdated(_changedProperties);
    this.loadCategories();
    // Extraer parte de la URL necesaria. Solo el ID.
    const movieUrl = this.selectedMovie.preview.split("/")[3];
    const movieControls =
      "?autoplay=1&controls=1&loop=1&disablekb=1&modestbranding=1&rel=0&showinfo=0&mute=0&enablejsapi=1&html5=1";
    this.selectedMovieURL = `https://www.youtube.com/embed/${movieUrl}${movieControls}`;

    if (typeof this.selectedMovie.category === "string") {
      var newHeaders = new Headers();
      newHeaders.append("x-access-token", sessionStorage.getItem("token"));

      var requestOptions = {
        method: "GET",
        headers: newHeaders,
      };

      fetch(
        "https://centraal.sfi.digital/api/v1/category/" +
          this.selectedMovie.category,
        requestOptions
      )
        .then((result) => result.json())
        .then((result) => {
          if (result.success) {
            this.selectedMovie.category = result.data;
            this.selectedMovie = { ...this.selectedMovie };
          }
        });
    }
  }

  playMovie() {
    this.videoPlayer = true;
  }

  render() {
    return html`
    <div><h1 class="title">${this.selectedMovie.title}</h1></div>
      <div class="container">
      
        <div>

          <img
            src="${this.selectedMovie.cover}"
            alt="${this.selectedMovie.title}"
          />
        </div>

        <div>
          
          
          <h4>
            ${this.selectedMovie.year}, ${this.selectedMovie.category.title}
          </h4>
          <p>Dirigida por: ${this.selectedMovie.directors}</p>
          <p class="description">${this.selectedMovie.description}</p>
          <div class="controls">
            <vaadin-button theme="secondary" @click="${this.playMovie}"
              ><vaadin-icon icon="vaadin:play"></vaadin-icon> Ver
              trailer</vaadin-button
            >
            <a @click="${this.backHome}">Volver atrás</a>
          </div>
        </div>
      </div>
      <div id="player" class="player">
        ${this.videoPlayer === true
          ? html` <iframe
              frameborder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              autoplay="1"
              encrypted-media
              allowfullscreen
              allowscriptaccess="always"
              src=${this.selectedMovieURL}
            ></iframe>`
          : ``}
      </div>
    `;
  }
}
